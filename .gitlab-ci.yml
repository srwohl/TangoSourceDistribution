# See https://docs.gitlab.com/ce/ci/yaml/README.html for the documentation

variables:
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}"
  CPPTANGO_MSVC_VERSION: "v141"

# See: https://docs.gitlab.com/ce/ci/yaml/README.html#workflowrules-templates
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

package:
  image: debian:bullseye
  tags:
    - docker, linux, amd64
  before_script:
    - apt-get update
    - >
        apt-get install --yes
        ant
        git
        make
        python3-pip
    - pip3 install -r https://gitlab.com/tango-controls/tango-doc/-/raw/9.3.5/requirements.txt
    - git config --global advice.detachedHead false
  script:
    - ant build package
  artifacts:
    when: on_success
    paths:
      - build/tango-*.tar.gz

test:
  image: debian:bullseye
  tags:
    - docker, linux, amd64
  needs:
    - package
  before_script:
    - apt-get update
    - >
        apt-get install --yes
        build-essential
        cmake
        git
        libcos4-dev
        libjpeg-dev
        libmariadb-dev
        libomniorb4-dev
        libomnithread4-dev
        libzmq3-dev
        mariadb-server
        openjdk-11-jre-headless
        omniidl
        sudo
    # install newer cppzmq
    - git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq
    - cmake -B /cppzmq/build -DCPPZMQ_BUILD_TESTS=OFF /cppzmq
    - make -C /cppzmq/build install
    # make the database accessible
    - service mariadb restart
    - sudo mysql -u root -e "SET PASSWORD = PASSWORD('secret');"
    - tar xaf build/tango*tar*
    - cd tango*
  script:
    - cmake -B build -S . -DMYSQL_ADMIN=root -DMYSQL_ADMIN_PASSWD=secret -DTSD_TAC=ON -DTDB_DATABASE_SCHEMA=ON
    - cmake --build build
    - sudo cmake --install build

release-builds:
  variables:
    WINDOWS_VERSION_INFO: "${CPPTANGO_MSVC_VERSION}_win64"
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - package
    - build-windows-installer
    - test
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
  before_script:
    - apk update
    - apk add curl
    - apk --no-cache add findutils
  script:
    - cd build
    - >
      find . -type f -printf "%f\n" -exec curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file {} "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/{}" \;
    - cd ../artifacts
    - >
      find . -type f -printf "%f\n" -exec curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file {} "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/{}" \;
  release:
    name: 'Release $CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
    description: 'Release $CI_COMMIT_TAG'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'Tango Source Distribution'
          url: "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/tango-${CI_COMMIT_TAG}.tar.gz"
        - name: 'Windows Installer'
          url: "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/tango-${CI_COMMIT_TAG}_${WINDOWS_VERSION_INFO}.exe"

.shared_windows_runners:
  tags:
    - shared-windows
    - windows
    - windows-1809

build-windows-installer:
  extends:
    - .shared_windows_runners
  needs:
    - package
  variables:
    ARCH: "x64"
    MYSQL_VERSION: "5.7"
    MYSQL_VERSION_PATCH: "36"
    GENERATOR_VERSION: "Visual Studio 16 2019"
    BUILD_CONFIGURATION: "Release"
    CPPTANGO_PROJECT_ID: "tango-controls%2FcppTango"

  script:
    - choco install cmake --version 3.22.3 --force -y
    - choco install innosetup --version 6.2.0 --force -y
    - choco install 7zip --version 22.0 --force -y
    - try { .\windows\create_installer.ps1 } catch { Write-Host -Foreground Red $_; Exit 1 }
    - $result = .\windows\create_installer.ps1 -Step GetArtifacts
    - New-Item artifacts -ItemType Directory
    - New-Item misc -ItemType Directory
    - Get-ChildItem -Name -Recurse -Force -File -Path $result['setupDir'] > misc/fileslist.txt
    - Copy-Item $result['issScript']  -Destination misc
    - Copy-Item $result['installer']  -Destination artifacts
    - Copy-Item windows/README.txt  -Destination artifacts

  artifacts:
    paths:
      - artifacts
      - misc
