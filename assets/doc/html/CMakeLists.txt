project(tango) # so that CMAKE_INSTALL_DOCDIR is correct

include(GNUInstallDirs)

install(DIRECTORY ./
        DESTINATION ${CMAKE_INSTALL_DOCDIR}
        PATTERN "CMakeLists.txt" EXCLUDE)
