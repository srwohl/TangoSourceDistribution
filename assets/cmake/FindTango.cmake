if (DEFINED Tango_FOUND)
    return()
endif()
set(Tango_FOUND TRUE)
message(STATUS "Using bundled libtango")

if (NOT TARGET Tango::Tango)
    add_library(_tsd_libtango INTERFACE)
    add_dependencies(_tsd_libtango tango)

    # Fix the properties of the tango target
    get_target_property(_tsd_inc_dirs tango INCLUDE_DIRECTORIES)
    if (_tsd_inc_dirs)
        target_include_directories(_tsd_libtango INTERFACE ${_tsd_inc_dirs})
    endif()
    unset(_tsd_inc_dirs)

    get_target_property(_tsd_link_dirs tango INTERFACE_LINK_LIBRARIES)
    if (_tsd_link_dirs)
        target_link_directories(_tsd_libtango INTERFACE ${_tds_link_dirs})
    endif()
    unset(_tsd_link_dirs)

    get_target_property(_tsd_link_libs tango INTERFACE_LINK_LIBRARIES)
    target_link_libraries(_tsd_libtango INTERFACE "$<TARGET_FILE:tango>" ${_tsd_link_libs})
    unset(_tsd_link_librs)

    get_target_property(_tsd_compile_opts tango INTERFACE_COMPILE_OPTIONS)
    if (_tsd_compile_opts)
        set_target_properties(_tsd_libtango PROPERTIES INTERFACE_COMPILE_OPTIONS ${_tsd_compile_opts})
    endif()
    unset(_tsd_compile_opts)

    get_target_property(_tsd_compile_defs tango INTERFACE_COMPILE_DEFINITIONS)
    if (_tsd_compile_defs)
        set_target_properties(_tsd_libtango PROPERTIES INTERFACE_COMPILE_DEFINITIONS ${_tsd_compile_defs})
    endif()
    unset(_tsd_compile_defs)

    add_library(Tango::Tango ALIAS _tsd_libtango)
endif()

